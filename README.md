[[_TOC_]]

## Relevant links

### Python client library for ROS (reference)

- ```rospy```: http://docs.ros.org/en/melodic/api/rospy/html/rospy-module.html
- ```rospy.Publisher```: http://docs.ros.org/en/melodic/api/rospy/html/rospy.topics.Publisher-class.html
- ```rospy.Subscriber```: http://docs.ros.org/en/melodic/api/rospy/html/rospy.topics.Subscriber-class.html

### Common ROS Messages

- Standard ROS Messages (**std_msgs**): http://wiki.ros.org/std_msgs 
- Geometric primitives (**geometry_msgs**): http://wiki.ros.org/geometry_msgs 
- Commonly used sensors (**sensor_msgs**): http://wiki.ros.org/sensor_msgs 
